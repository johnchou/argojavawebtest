package com.slalom.national.template.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author garyk
 * Simple REST controller used as a sample.  Built on Spring
 */
@Controller
@RequestMapping("/")
public class BaseController {

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(ModelMap map) {
		map.addAttribute("heading", "Sample REST App!");
		map.addAttribute("message", "This is a sample REST Service. Try adding /welcome at the end of the url for another service!");
		return "index";
	}
	
	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	public String welcome(ModelMap map) {
		map.addAttribute("heading", "Hello World!");
		map.addAttribute("message", "Welcome to a sample REST Service built on Spring");
		return "index";
	}

	@RequestMapping(value = "/health", method = RequestMethod.GET)
	public String health(ModelMap map) {
		map.addAttribute("heading", "Health Page!");
		map.addAttribute("message", "This is a sample REST Service is up and running. Try adding /welcome at the end of the url for another service!");
		return "index";
	}
}
